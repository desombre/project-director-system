package re.desomb.pds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectDirectorSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectDirectorSystemApplication.class, args);
	}

}
